FROM mambaorg/micromamba

WORKDIR /app 
COPY env.yml /app/env.yml

# Создание и активация окружения для разработки машинного обучения
RUN micromamba create -f env.yml

#COPY pyproject.toml /app/pyproject.toml
#COPY poetry.lock /app/poetry.lock

#RUN micromamba run -n mlops4block poetry install --with dev
# COPY . /app/

# Установка точки входа для запуска приложения в созданном окружении
# ENTRYPOINT [ "micromamba", "run", "-n", "fast_dev_ml_env", "--no-capture-output", "python", "/mlops4block/hello.py" ]
